-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: app
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'client_01',NULL,NULL),(2,'client_02',NULL,NULL),(3,'client_03',NULL,NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `headercards`
--

DROP TABLE IF EXISTS `headercards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `headercards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `headercards`
--

LOCK TABLES `headercards` WRITE;
/*!40000 ALTER TABLE `headercards` DISABLE KEYS */;
INSERT INTO `headercards` VALUES (1,'Galería','store','nc-album-2','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:33:21'),(2,'Portafolio','store','nc-layout-11','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:34:00'),(3,'Testimonios','store','nc-chat-33','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:35:00'),(4,'Tienda','store','nc-cart-simple','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:35:27'),(5,'Ofertas','service','nc-alert-circle-i','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:37:14'),(6,'Promociones','service','nc-bookmark-2','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:37:47'),(7,'Paquetes','service','nc-box-2','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:38:22'),(8,'Productos','service','nc-bullet-list-67','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.','Ver Más',NULL,'2017-09-24 23:38:45');
/*!40000 ALTER TABLE `headercards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (10,'2017_09_20_201338_create_tasks_table',1),(17,'2014_10_12_000000_create_users_table',2),(18,'2014_10_12_100000_create_password_resets_table',2),(19,'2017_09_21_063916_create_tasks_table',2),(20,'2017_09_21_212424_create_pages_table',3),(24,'2017_09_22_060803_create_quotes_table',4),(25,'2017_09_22_061027_create_headercards_table',4),(27,'2017_09_22_081141_create_clients_tables',5),(28,'2017_09_22_094326_create_testimonials_table',6),(29,'2017_09_22_094351_create_teams_table',6),(30,'2017_09_24_223001_create_products_table',7),(31,'2017_09_25_025859_create_shop_cards_table',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'project_1_portfolio','service','Vail Boards','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',NULL,NULL),(2,'project_2','gear','Frostbite Climbing Gear','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',NULL,NULL),(3,'project_3','product','Custom Skis','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',NULL,NULL),(4,'project_4','product','High Tech Enviroment Footwear','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',NULL,NULL),(5,'project_5','product','Premium Equipment','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CO` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes`
--

LOCK TABLES `quotes` WRITE;
/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
INSERT INTO `quotes` VALUES (0,'quote','Si tu negocio no está en Internet, no existe.','Bill Gates','Microsoft',NULL,NULL),(1,'quote','Ve en grande o vete a casa.','Anónimo','',NULL,NULL);
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_cards`
--

DROP TABLE IF EXISTS `shop_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_cards`
--

LOCK TABLES `shop_cards` WRITE;
/*!40000 ALTER TABLE `shop_cards` DISABLE KEYS */;
INSERT INTO `shop_cards` VALUES (1,'Shoes','male','shoe-(1)','Pathfinder Boots','2.900','shoes',NULL),(3,'Jackets','male','jacket (8)','Chrystal Windbreaker','1.500','clothes',NULL),(4,'Shoes','female','shoe-(3)','Hilltop Shoes','3.500','shoes',NULL),(5,'Jackets','female','jacket (4)','Stretch-Knit Jacket','2.600','clothes',NULL),(6,'Jackets','female','jacket (7)','Glass Beads Vest','7.500','clothes',NULL),(7,'Jackets','male','jacket (1)','Grey Velvet Windbreaker','1.300','clothes',NULL),(9,'Jackets','female','jacket (6)','Purple Orchid Climbing Coat','3.700','clothes',NULL),(10,'Shoes','male','shoe-(2)','Ash Lake Hikers','4.500','shoes',NULL);
/*!40000 ALTER TABLE `shop_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'duty','Learn PHP',1,'2017-09-21 11:22:28','2017-09-21 11:22:28'),(2,'need','Sleep',0,'2017-09-21 07:28:30','2017-09-21 07:28:30'),(3,'need','Wake Up',0,'2017-09-23 11:04:07','2017-09-23 11:26:55');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Sophie West','CEO','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',NULL,NULL),(2,'Durk Jones','Director','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',NULL,NULL),(3,'Robert Orben','Ventas','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',NULL,NULL);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `thought` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a.\"','Gina Andrew','@ginaandrew',NULL,NULL),(2,'\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a.\"','Joe West','@georgewest',NULL,NULL),(3,'\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a.\"','Alec Thompson','@alecthompson',NULL,NULL);
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-26  7:30:37
