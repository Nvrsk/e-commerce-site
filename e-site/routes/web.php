<?php



Route::get('terms', array('as' => 'terms',function () {

	return view('pages.terms');
} 
));

Route::get('settings', array('as' => 'settings',function () {

	return view('pages.settings');
} 
));

Route::get('register', array('as' => 'register',function () {

	return view('pages.register');
} 
));

Route::get('refund', array('as' => 'refund',function () {

	return view('pages.refund');
} 
));

Route::get('profile', array('as' => 'profile',function () {

	return view('pages.profile');
} 
));


Route::get('privacy', array('as' => 'privacy',function () {

	return view('pages.privacy');
} 
));


Route::get('news', array('as' => 'news',function () {

	return view('pages.news');
} 
));


Route::get('login', array('as' => 'login',function () {

	return view('pages.login');
} 
));


Route::get('gallery', array('as' => 'gallery',function () {

	return view('pages.gallery');
} 
));


Route::get('contact', array('as' => 'contact',function () {

	return view('pages.contact');
} 
));


Route::get('about', array('as' => 'about',function () {

	return view('pages.about');
} 
));


Route::get('checkout', array('as' => 'checkout',function () {

	return view('pages.checkout');
} 
));


Route::get('shopping-cart', array('as' => 'shopping-cart',function () {

	return view('pages.shopping-cart');
} 
));;

Route::get('store-product', array('as' => 'store-product',function () {

	return view('pages.store-product');
} 
));;

Route::get('/store-home', 'ShopController@index')->name('store-home');

Route::get('/store', 'ShopController@shop')->name('store');

Route::get('/{products}', 'IndexController@show');

Route::get('/', 'IndexController@index')->name('index');









