    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Demo Home</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" name="vier">
        <meta name="viewport" content="width=device-width" />

        <link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('assets/css/paper-kit-mixings.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('assets/css/demo.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{URL::asset('assets/css/custom.css')}}">

        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Josefin+Sans|Just+Another+Hand" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{URL::asset('assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{URL::asset('assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/css/nucleo-icons.css')}}">


    </head>



        @yield('body')

 
    <!-- Core JS Files -->
    <script src="{{URL::asset('assets/js/jquery-3.2.1.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>


    <!-- Switches -->
    <script src="{{URL::asset('assets/js/bootstrap-switch.min.js')}}"></script>

    <!--  Plugins for Slider -->
    <script src="{{URL::asset('assets/js/nouislider.js')}}"></script>

    <!--  Plugins for DateTimePicker -->
    <script src="{{URL::asset('assets/js/moment.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>

    <!--  Paper Kit Initialization and functons -->
    <script src="{{URL::asset('assets/js/paper-kit.js?v=2.0.0')}}"></script>
    <!-- custom script -->

    <!-- Smoothscrolling script -->
     @yield('script')
    <!-- Filter script -->
    <script>
      $(document).ready(function(){
 
    $(".category-button").click(function(){

        var filterValue = $(this).attr('data-filter');

        if(filterValue == "all")
        {
            $(".all").show("slow");
        }
        else
        {   

            $(".all").not('.'+filterValue).hide("slow");

            $(".all").filter('.'+filterValue).show("slow");
        }
    });

});
    </script>

    </html>
