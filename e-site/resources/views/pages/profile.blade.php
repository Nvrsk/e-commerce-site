        @extends ('layout')


        @section('body')

        <body>



            @include('partials.navbar')



            <div class="wrapper">
                <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('assets/img/cover.jpg');">
                   <div class="filter"></div>
               </div>
               <div class="section profile-content">
                <div class="container">
                    <div class="owner">
                        <div class="avatar">
                            <img src="assets/img/faces/5.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                        </div>
                        <div class="name">
                            <h4 class="title">Joe Faker<br /></h4>
                            <h6 class="description">Do some stuff</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 offset-md-3 text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae autem tempore minus vel fuga ratione recusandae magnam, nulla provident et quidem. Vel nam, qui temporibus odio hic labore, reiciendis praesentium. </p>
                            <br />
                            <a href="{{URL::route('settings')}}"><btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Settings</btn></a>
                        </div>
                    </div>
                </div>
            </div>



            <div class="section">
                <div class="container">
                 <div class="row">
                    <div class="col-md-12">
                       <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">Ordenes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">Direcciones de Envío</a>
                        </li>
                           <li class="nav-item">
                            <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment">Formas de Pago</a>
                        </li>
                  </ul>
                  <br>

                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row  pt-4 pl-4" style="border:1px #ccc solid;">
                          <div class="col-md-4">
                              <h6>Fecha</h6>
                              <p>Julio 12, 2017</p>
                          </div>

                          <div class="col-md-4">
                              <h6>Total</h6>
                              <p><strong>$</strong> 199.99</p>
                          </div>

                          <div class="col-md-4 ">

                            <h6>Nº de Orden: #0000-0000000-00</h6>
                            <div class="row">



                              <a href=""><button class="btn btn-link btn-primary">
                                 Detalles
                             </button></a>


                             <a href=""><button class="btn btn-link btn-primary">
                                 Factura
                             </button></a>
                         </div>

                     </div>




                 </div>
                 <div class="row pt-4 pl-4 pb-2" style="border:1px #ccc solid;">
                     <div class="col-md-4">
                      <img src="https://placehold.it/100x100" alt="" class="thumbnail img-fluid">
                  </div>
                  <div class="col-md-4">
                    <div class="row">  <a href="">
                     <p>Articulo</p>

                 </a></div>
                 <div class="row">

                    <p>Tipo</p>
                </div>
                <div class="row">
                 <p>Precio</p>
             </div>
                </div>

             <div class="col-md-4">
                <div class="row text-left"><div class="col-md-12">
                   <a href=""><button type="button" class="btn btn-link btn-primary">Devolver</button></a>
               </div>
           </div>
           <div class="row text-left"><div class="col-md-12">
            <a href=""><button type="button" class="btn btn-link btn-primary">Opinión</button></a>
        </div>
    </div>
    <div class="row text-left"><div class="col-md-12">
        <a href=""><button type="button" class="btn btn-link btn-primary">Eliminar</button></a>
    </div>
    </div>


    </div>
         </div>
    </div>
 
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="row">
                <div class="col-md-4 default">        
                    <div class="card" style="border:1px #ccc solid; border-radius:0;"    >
                                   
                                            <div class="card-block">
                                                <h6 class="card-category text-info">Por defecto</h6>
                                                <h5 class="card-title">
                                                    Joe Faker
                                                </h5>
                                                <p class="card-description pl-3">
                                                    <span class="row">3300 Nw 112th Ave Unit 10</span>
                                                    <span class="row">Miami, Fl 33172-5059</span>
                                                    <span class="row">United States</span>
                                                    <span class="row">Phone number: 305-599-9960</span>
                                                   


                                                <br>
                                                </p>
                                                <hr>
                                                <div class="card-footer">
                                               <div class="row">
                                                   <div class="col-md-4"><a href="">Editar</a></div>
                                                   <div class="col-md-4"><a href="">Eliminar</a></div>
                                                   <div class="col-md-4"><a href=""></a></div>
                                               </div>
                                                </div>
                                            </div>
                                        </div>
                </div>
                  <div class="col-md-4">        
                    <div class="card" style="border:1px #ccc solid; border-radius:0;"    >
                                   
                                            <div class="card-block">
                                                <h6 class="card-category text-info">adicional</h6>
                                                <h5 class="card-title">
                                                    Jhon Doe
                                                </h5>
                                                <p class="card-description pl-3">
                                                    <span class="row">8610 Nw 72nd St</span>
                                                    <span class="row">Miami, Fl 33172-5059</span>
                                                    <span class="row">United States</span>
                                                    <span class="row">Phone number: 305-599-9960</span>
                                                   


                                                <br>
                                                </p>
                                                <hr>
                                                <div class="card-footer">
                                               <div class="row">
                                                   <div class="col-md-4"><a href="">Editar</a></div>
                                                   <div class="col-md-4"><a href="">Eliminar</a></div>
                                                   <div class="col-md-4"><a href="">Establecer</a></div>
                                               </div>
                                                </div>
                                            </div>
                                        </div>
                </div>
   

            </div>
    </div>
    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
        <div id="exampleAccordion" data-children=".item">
  <div class="item">
    <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion1" aria-expanded="true" aria-controls="exampleAccordion1">
      <h6>Tarjetas: </h6>
    </a>
    <div id="exampleAccordion1" class="collapse show pt-4 pb-4" role="tabpanel">
            <div class="row  pt-4 pl-4 pb-3" style="border:1px #ccc solid;">
              <div class="col-md-4">
                  <h6>Tipo</h6>
                  <span><i class="fa fa-cc-visa fa-2x"></i></span>
              </div>
                   <div class="col-md-4">
                  <h6>Fecha Exp.</h6>
                  <p>07 / 2020</p>
              </div>

              <div class="col-md-4 ">

                <h6>Nº de Tarjeta : 4400-0000-0000-0000</h6>
                <div class="row">



                  <a href=""><button class="btn btn-link btn-primary">
                   Editar
               </button></a>


               <a href=""><button class="btn btn-link btn-primary">
                   Eliminar
               </button></a>
           </div>

       </div>





    </div>
    </div>
  </div>
  <div class="item">
    <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion2" aria-expanded="false" aria-controls="exampleAccordion2">
      <h6>Otras formas de pago:</h6>
    </a>
    <div id="exampleAccordion2" class="collapse" role="tabpanel">
      <p class="mb-3">
        Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      </p>
    </div>
  </div>
</div>
    </div>
    </div>

    </div>

    </div>



    </div>

    </div>
    </div>

    <br/>

    </div>


    @include('partials.footer')

    </body>




    @endsection