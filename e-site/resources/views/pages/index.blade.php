@extends ('layout')


@section('body')
<body class="presentation-page loading">


    @include('partials.navbar')


    <div class="wrapper">
       <!-- header -->
       <div class="page-header" data-parallax="true" style="background-image: url(assets/img/pk.jpg);">
        <div class="filter"></div>
        <div class="container">

         <div class="motto text-center">
           <div class="title-brand"><h1><img src="assets/img/Logo.png" alt="logo" class="img-fluid"></h1>
            <div class="fog-low"><img src="assets/img/fog-low.png" alt=""></div>
            <div class="fog-low right"><img src="assets/img/fog-low.png" alt=""></div>
        </div>
        @foreach ($quote as $quote)
        <blockquote class="blockquote" style="border: none;">
            <p class="mb-0">{{$quote->body}}</p>
            <footer class="blockquote-footer" style="color: white;"> {{$quote->author}} <cite title="Source Title"> {{$quote->CO}}</cite></footer>
        </blockquote>
        @endforeach
        <br>
        <a href="#" class="btn btn-danger">Nuevos Productos - Notícias</a>
        
    </div>
</div>
<div class="moving-clouds" style="background-image: url(assets/img/clouds.png);"></div>
</div>
<!-- end header -->

<!-- main content -->
<div class="main">
    <!-- section one -->
    <div class="section section-dark text-center">
        <div class="container">

            <div class="row">
               @foreach ($cards as $cards)
               <div class="col-md-3">
                  
                  
                 <div class="card card-header"  data-background="image" style="background-image: url(assets/img/card_0{{$cards->id}}.jpg);background-position: left;  ">
                    
                   
                    <div class="card-block">





                        <div class="card-icon icon-danger ">
                            <h1><i class="nc-icon {{$cards->icon}}"></i></h1>
                        </div>
                        <div class="card-description">
                            <h6 class="card-category">{{$cards->title}}</h6>
                            <p class="card-description">{{$cards->excerpt}}</p>

                        </div>
                        <div class="card-footer">
                          <a href="#pkp" class="btn btn-link btn-danger">{{$cards->link}}</a>  
                      </div>



                  </div>            
              </div>
          </div>

          @endforeach
      </div>

      
      <br/><br/>
      <div class="row">
        <div class="col-md-8 offset-md-2">
            <h5 class="title mb-2" style="text-transform: uppercase; font-weight: 900;">Descripción del producto o servicio</h5>
            <h5 class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi sequi veritatis reprehenderit laboriosam maxime neque, necessitatibus vero, ex beatae eum, assumenda dolor!</h5>
            <br>
            <a href="about-us.html" class="btn btn-danger btn-round">Quiénes Somos</a>
        </div>
    </div>
</div>
</div>
<!-- end section one     -->
<!-- section clients -->
<div class="section section-gold">
    <div class="container">
        <div class="row text-center">
          
            @foreach ($img as $img)

            <div class="col"><img src="assets/img/logo_{{$img->id}}.png" alt="{{$img->alt}}"></div>

            @endforeach
        </div>
    </div>
    <div id="portfolio" ></div>
</div >
<!-- end section clients -->

<!-- section portfolio -->
<div class="section section-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
               <span class="label label-danger"><strong>Nuestros Productos</strong></span>
               <hr>
               <h5 class="description">    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque atque id, dolor voluptatibus, tempore, ducimus necessitatibus.</h5>
           </div>
       </div>
       <div class="space-top"></div>
       <div class="row">
        @foreach ($product as $product)
        <div class="col-md-5 offset-md-1">
            <a href="/{{$product->id}}">
                <div class="card" data-background="image" style="background-image: url('assets/img/{{$product->img}}.jpg');" data-toggle="tooltip" data-placement="left" title="Hacer click para redirigir a otra pagina">
                    <div class="card-block">
                       
                    </div>
                    
                </div>
            </a>
        </div>

        <div class="col-md-5">
            <div class="card card-plain">
                <div class="card-block">
                    <h6 class="card-category">{{$product->class}} (Demo Pagina Individual)</h6>
                    <a href="portfolio-product.html"><h3 class="card-title">{{$product->title}}</h3></a>
                    <p class="card-description">{{$product->body}}</p>

                    <div class="card-footer">
                        <a href="#pablo" class="btn btn-link btn-neutral">
                            <i class="fa fa-book" aria-hidden="true"></i> Leer Más
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-apple" aria-hidden="true"></i>
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-android" aria-hidden="true"></i>
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-windows" aria-hidden="true"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        @endforeach
    </div>
    <br><hr><br>
    <div class="row">
        <div class="col-md-5 offset-md-1">
            <div class="card card-plain">
                <div class="card-block">
                    <h6 class="card-category">Tecnología (Demo Modal)</h6>
                    <a href="#" data-toggle="modal" data-target="#noticeModal">
                        <h3 class="card-title">Frostbite Climbing Gear</h3>
                    </a>
                    <p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti saepe, sit, inventore enim dolore obcaecati culpa ipsam excepturi velit dolores dignissimos eum soluta impedit temporibus debitis a consequatur deserunt autem....</p>

                    <div class="card-footer">
                        <a href="#pablo" class="btn btn-link btn-neutral">
                            <i class="fa fa-book" aria-hidden="true"></i> Leer Más
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-apple" aria-hidden="true"></i>
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-android" aria-hidden="true"></i>
                        </a>
                        <a href="#pablo" class="btn btn-just-icon btn-link btn-neutral">
                            <i class="fa fa-windows" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
         <a href="#" data-toggle="modal" data-target="#noticeModal"> 
            <div class="card" data-background="image" style="background-image: url('assets/img/project_2.jpg')"data-toggle="tooltip" data-placement="right" title="Hacer click para lanzar el modal">
                <div class="card-block">
                    
                </div>
            </div>
        </a>
    </div>
</div>
</div>
</div>
<div class="section section-dark">

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <span class="label label-danger"><strong>Otros Productos</strong></span>
                <hr>

                <h5 class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste quidem minus, ab voluptates nemo corporis voluptatem.</h5>
            </div>
        </div>
        <div class="space-top"></div>
        
        <div class="row">
            @foreach ($thumb_products as $thumb_product)
            <div class="col-md-4">
                <a href="/{{$thumb_product->id}}">
                    <div class="card card-plain">
                        <div class="card-img-top">
                            
                            <img class="img" src="assets/img/{{$thumb_product->img}}.jpg">
                            
                        </div>
                        <div class="card-block">
                            
                            <h6 class="card-category text-muted">{{$thumb_product->title}}</h6>
                            <p class="card-description">{{$thumb_product->body}}</p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        
    </div>
    
</div>
<!-- end section portfolio -->
<!-- section two -->
<div class="pricing-5 section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h6 class="title">Elige un plan para tu próximo proyecto</h6>
                <ul class="nav nav-pills nav-pills-danger" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#personal" role="tab" aria-expanded="false">Personal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#commercial" role="tab" aria-expanded="true">Comercial</a>
                    </li>
                </ul>
                <br>
                <p class="description text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facilis repellendus totam sed molestias qui sapiente.</p>
            </div>

            <div class="col-md-7 offset-md-1">
                <div class="tab-content text-center">
                    <div class="tab-pane" id="personal" role="tabpanel" aria-expanded="false">
                        <div class="space-top"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-pricing">
                                    <div class="card-block">
                                        <h6 class="card-category text-primary">Paquete Uno</h6>
                                        <h1 class="card-title">$0</h1>
                                        <ul>
                                            <li><b>1</b> Clientes</li>
                                            <li><b>99+</b> Componentes</li>
                                            <li><b>+10</b> Utilidades</li>
                                            <li><b>14</b> Ejemplos</li>
                                        </ul>
                                        <a href="#pablo" class="btn btn-primary btn-round">Descarga Gratuita</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-pricing" data-color="orange">
                                    <div class="card-block">
                                        <h6 class="card-category text-success">Paquete Dos</h6>
                                        <h1 class="card-title">$59</h1>
                                        <ul>
                                            <li><b>5</b> Clientes</li>
                                            <li><b>129+</b> Componentes</li>
                                            <li><b>+50</b> Elementos</li>
                                            <li><b>24</b> Ejemplos</li>
                                        </ul>
                                        <a href="#pablo" class="btn btn-neutral btn-round">Compra Ahora!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane active" id="commercial" role="tabpanel" aria-expanded="true">
                        <div class="space-top"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-pricing">
                                    <div class="card-block">
                                        <h6 class="card-category text-warning">Paquete Tres</h6>
                                        <h1 class="card-title">$159</h1>
                                        <ul>
                                            <li><b>1</b> Clientes</li>
                                            <li><b>99+</b> Componentes</li>
                                            <li><b>+10</b> Utilidades</li>
                                            <li><b>14</b> Ejemplos</li>
                                        </ul>
                                        <a href="#pablo" class="btn btn-warning btn-round">Compra Ahora!</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card card-pricing" data-color="orange">
                                    <div class="card-block">
                                        <h6 class="card-category text-success">Paquete Cuatro</h6>
                                        <h1 class="card-title">$359</h1>
                                        <ul>
                                            <li><b>5</b> Clientes</li>
                                            <li><b>129+</b> Componentes</li>
                                            <li><b>+50</b> Elementos</li>
                                            <li><b>24</b> Ejemplos</li>
                                        </ul>
                                        <a href="#pablo" class="btn btn-neutral btn-round">Compra Ahora!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section two -->
<!-- section three -->
<div class="cd-section section-white" id="testimonials">

    <!--     *********    TESTIMONIALS 1     *********      -->

    <div class="testimonials-1 section-dark">
        <div class="filter"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <span class="label label-danger"><strong>Testimonios</strong></span>
                    <hr>
                    <h5 class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, mollitia, totam. Doloribus tempora repellat maiores optio dolor ipsa ad. A voluptas adipisci excepturi doloribus veniam at eum, aperiam facilis. Ipsam.</h5>
                </div>
            </div>
            <div class="space-top"></div>
            
            <div class="row"> 
                @foreach ($testimonials as $testimonial)
                <div class="col-md-4">
                    <div class="card card-testimonial">
                        <div class="card-icon">
                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>
                        <div class="card-block">
                            <p class="card-description">
                                {{$testimonial->thought}}
                            </p>
                            <div class="card-footer">
                                <h4 class="card-title">{{$testimonial->name}}</h4>
                                <h6 class="card-category">{{$testimonial->social}}</h6>
                                <div class="card-avatar">
                                    <a href="#pablo">
                                        <img class="img" src="assets/img/faces/{{$testimonial->id}}.jpg">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            
            

        </div>
    </div>


    
</div>
<!-- end section three -->
<!-- section four -->

<div class="section text-center">
    <div class="container">
        <span class="label label-danger"><strong>Equipo</strong></span>
        <hr>
        
        <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis in qui necessitatibus quae aperiam, ratione facere ad itaque, illum magni, iusto autem tempora odio vel. Itaque nisi autem culpa ducimus!</h5>
        <!--     <div class="separator"><i class="pe-7s-network" style="font-size: 2em;"></i></div> -->
        <div class="row">
            @foreach ($teams as $team)

            <div class="col-md-4">
                <div class="card card-profile card-plain">
                    <div class="card-avatar">
                        <a href="#avatar"><img src="assets/img/faces/{{$team->id}}.jpg" alt="{{$team->name}}"></a>
                    </div>
                    <div class="card-block">
                        <a href="#paper-kit">
                            <div class="author">
                                <h4 class="card-title">{{$team->name}}</h4>
                                <h6 class="card-category">{{$team->role}}</h6>
                            </div>
                        </a>
                        <p class="card-description text-center">
                            {{$team->resume}}
                        </p>
                    </div>
                    <div class="card-footer text-center">
                        <a href="#pablo" class="btn btn-link btn-just-icon btn-success"><i class="fa fa-twitter"></i></a>
                        <a href="#pablo" class="btn btn-link btn-just-icon btn-success"><i class="fa fa-google-plus"></i></a>
                        <a href="#pablo" class="btn btn-link btn-just-icon btn-success"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div>
<!-- end section four -->

</div>
<!-- end main content -->

</div>




<!-- modals -->

@include('partials.simple-modal')

@include('partials.demo-modal')

<!-- end modals -->



@include('partials.footer')

</body>

@endsection

@section('script')

 <script>

    $('a[href*="#"]')
     
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {

        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
  
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        
          if (target.length) {
    
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {

      
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { 
                return false;
              } else {
                $target.attr('tabindex','-1'); 
                $target.focus(); 
              };
            });
          }
        }
      });
    </script>


@endsection