@extends ('layout')
    

@section('body')

    <body>



    @include('partials.navbar')


<div class="wrapper">
        <div class="page-header page-header-xs" style="background-image: url(assets/img/cover.jpg);">
      <div class="filter"></div>
      <div class="content-center mt-0">
        <div class="container">
          <div class="title-brand">
            <div class="presentation-title">
              <h4>TU LISTA DE COMPRAS</h4>
              


            </div>


          </div>

        </div>
      </div>
    </div>
 <div class="main">
   <div class="section section-white">
        <div class="container">
        <div class="row">
                <div class="col-md-12 text-center">
                    <h6 class="title">Productos</h6>
                </div>
                <div class="col-md-10 offset-md-1">
                    <div class="table-responsive">
                    <table class="table table-shopping">
                        <thead class="thead-inverse">
                            <tr>
                                <th class="text-center"></th>
                                <th></th>
                                <th class="text-right">Precio</th>
                                <th class="text-right">Cantidad</th>
                                <th class="text-right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="img-container">
                                        <img src="assets/img/shoes (2).jpg" alt="Agenda">
                                    </div>
                                </td>
                                <td class="td-product">
                                    <strong>Hilltop Shoes</strong>
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum adipisci incidunt.</p>
                                </td>

                                <td class="td-price">
                                    <small>€</small>149
                                </td>
                                <td class="td-number td-quantity">
                                    1
                                    <div class="btn-group">
                                        <button class="btn btn-sm btn-border btn-round"> - </button>
                                        <button class="btn btn-sm btn-border btn-round"> + </button>
                                    </div>
                                </td>
                                <td class="td-number">
                                    <small>€</small>149
                                </td>
                            </tr>
                            <tr>
                                 <td>
                                    <div class="img-container">
                                        <img src="assets/img/jacket (4).jpg" alt="Stylus">
                                    </div>
                                </td>
                                <td class="td-product">
                                    <strong>Stretch-Knit Jacket</strong>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut voluptate.</p>
                                </td>

                                <td class="td-price">
                                    <small>€</small>2.600
                                </td>
                                <td class="td-number td-quantity">
                                    2
                                    <div class="btn-group">
                                        <button class="btn btn-sm btn-border btn-round"> - </button>
                                        <button class="btn btn-sm btn-border btn-round"> + </button>
                                    </div>
                                </td>
                                <td class="td-number">
                                    <small>€</small>5.200
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-container">
                                        <img src="assets/img/aditional_product.jpg" alt="Evernote">
                                    </div>
                                </td>
                                <td class="td-product">
                                    <strong>Army Tactical Net Scarf</strong>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                                </td>

                                <td class="td-price">
                                    <small>€</small>975 
                                </td>
                                <td class="td-number td-quantity">
                                    1
                                    <div class="btn-group">
                                        <button class="btn btn-sm btn-border btn-round"> - </button>
                                        <button class="btn btn-sm btn-border btn-round"> + </button>
                                    </div>
                                </td>
                                <td class="td-number">
                                    <small>€</small>975 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td></td>
                                <td class="td-total">
                                   Total
                                </td>
                                <td class="td-total">
                                    <small>€</small>12,999
                                </td>
<!--
                                <td> <button type="button" class="btn btn-info btn-l">Complete Purchase <i class="fa fa-chevron-right"></i></button></td>

                                <td></td>
-->
                            </tr>
                            <tr class="tr-actions">
                               <td colspan="3"></td>
                               <td colspan="2" class="text-right">
                                   <a href="{{ URL::route('checkout') }}"><button type="button" class="btn btn-danger btn-lg">Completar Pedido <i class="fa fa-chevron-right"></i></button></a>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>

                </div>
            </div>
    </div>
   </div>
 </div>
</div>



<!-- Modal Bodies come here -->

<!--   end modal -->
    @include('partials.simple-modal')


    <!-- end modals -->


    
    @include('partials.footer')

    </body>




@endsection