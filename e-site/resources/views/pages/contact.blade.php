@extends ('layout')


@section('body')
<body>



    @include('partials.navbar')
    
    <div class="wrapper">
       <div class="main" >
         <div class="contactus-1 section-image" style="background-image: url('assets/img/cover.jpg')">
          <div class="container mt-5">
             <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="card card-contact no-transition">
                        <h3 class="card-title text-center">Contact Us</h3>
                        <div class="row">
                            <div class="col-md-5 offset-md-1">
                                <div class="card-block">
                                    <div class="info info-horizontal">
                                        <div class="icon icon-info">
                                            <i class="nc-icon nc-pin-3" aria-hidden="true"></i>
                                        </div>
                                        <div class="description">
                                            <h4 class="info-title">Find us at the office</h4>
                                            <p style="color: #000000;"> Bld Mihail Kogalniceanu, nr. 8,<br>
                                                7652 Bucharest,<br>
                                                Romania
                                            </p>
                                        </div>
                                    </div>
                                    <div class="info info-horizontal">
                                        <div class="icon icon-danger">
                                            <i class="nc-icon nc-mobile" aria-hidden="true"></i>
                                        </div>
                                        <div class="description">
                                            <h4 class="info-title">Give us a ring</h4>
                                            <p style="color: #000000;"> Mike Jordan<br>
                                                +40 762 321 762<br>
                                                Mon - Fri, 8:00-22:00
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <form role="form" id="contact-form" method="post">
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">First name</label>
                                                    <input type="text" name="name" class="form-control" placeholder="First Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Last name</label>
                                                    <input type="text" name="name" class="form-control" placeholder="Last Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Email address</label>
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Your message</label>
                                            <textarea name="message" class="form-control" id="message" rows="6" placeholder="Message"></textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <input id="checkbox1" type="checkbox">
                                                    <label for="checkbox1">
                                                        I'm not a robot !
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary pull-right">Send Message
                                                </button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="modal" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog modal-notice pt-1">
        <div class="modal-content">
            <div class="modal-header no-border-header">
                <h5 class="modal-title" id="myModalLabel">Item Detailed Info </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-8">
                            <p> <strong><h6 class="title">1. Front </h6></strong>  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur perferendis reprehenderit iure sed.</p>
                        </div>
                        <div class="col-md-4">
                            <div class="picture">
                                <img src="https://placehold.it/200x200?text=Front" alt="Thumbnail Image" class="img-rounded img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-8">
                            <p> <strong><h6 class="title">2. Rear </h6></strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum hic amet officiis id ducimus eius nulla adipisci libero debitis.</p>
                        </div>
                        <div class="col-md-4">
                            <div class="picture">
                                <img src="https://placehold.it/200x200?text=Rear" alt="Thumbnail Image" class="img-rounded img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-link" data-dismiss="modal"><i class="nc-icon nc-cart-simple"></i><h6>Agregar al carrito</h6></button>
            </div>
        </div>
    </div>
</div>



@include('partials.footer')

</body>




@endsection
