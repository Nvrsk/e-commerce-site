@extends ('layout')


@section('body')
<body>



    @include('partials.navbar')
    <div class="header-wrapper">

        <div class="page-header page-header-small" style="background-image: url('assets/img/us.jpg');">
            <div class="filter "></div>
            <div class="content-center">
                <div class="container">
                    <h1 class="presentation-title" style="font-weight: 900;text-transform: uppercase;">Somos E-COMMERCE</h1>
                    <h3>Déjanos contarte un poco más acerca de lo que hacemos</h3>
                </div>
            </div>
        </div>
    </div>




    <!-- end navbar  -->
    <div class="wrapper">
        <div class="main">
            <div class="section">
                <div class="container">
                    <h3 class="title-uppercase">We sell great products.</h3>
                    <p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>

                    <p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions. </p>

                    <h3 class="title-uppercase">We love</i>&nbsp; what we do.</h3>
                    <p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.</p>

                    <h3 class="more-info">Need more information?</h3>
                    <br>
                    <div class="row coloured-cards">
                        <div class="col-md-4 col-sm-6">
                            <div class="card-big-shadow">
                                <div class="card card-just-text" data-background="color" data-color="blue" data-radius="none">
                                    <div class="card-block">
                                        <h6 class="card-category">Best cards</h6>
                                        <h4 class="card-title"><a href="#paper-kit">Blue Card</a></h4>
                                        <p class="card-description">What all of these have in common is that they're pulling information out of the app or the service and making it relevant to the moment. </p>
                                    </div>
                                </div> <!-- end card -->
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="card-big-shadow">
                                <div class="card card-just-text" data-background="color" data-color="green" data-radius="none">
                                    <div class="card-block">
                                        <h6 class="card-category">Best cards</h6>
                                        <h4 class="card-title"><a href="#paper-kit">Green Card</a></h4>
                                        <p class="card-description">What all of these have in common is that they're pulling information out of the app or the service and making it relevant to the moment. </p>
                                    </div>
                                </div> <!-- end card -->
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="card-big-shadow">
                                <div class="card card-just-text" data-background="color" data-color="yellow" data-radius="none">
                                    <div class="card-block">
                                        <h6 class="card-category">Best cards</h6>
                                        <h4 class="card-title"><a href="#paper-kit">Yellow Card</a></h4>
                                        <p class="card-description">What all of these have in common is that they're pulling information out of the app or the service and making it relevant to the moment. </p>
                                    </div>
                                </div> <!-- end card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

</body>

@endsection
