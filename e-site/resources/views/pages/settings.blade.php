@extends ('layout')
    

@section('body')

    <body class="settings">



    @include('partials.navbar')
	<div class="page-header page-header-xs " style="background-image: url('assets/img/login-image.jpg');">
            <div class="filter"></div>
        </div>
	<div class="wrapper">
        
        <div class="profile-content section">
            <div class="container">
                <div class="row">
                    <div class="profile-picture">
                        <div class="fileinput fileinput-new mt-4" data-provides="fileinput">
                            <div class="fileinput-new img-no-padding">
                                <img src="assets/img/faces/joe-doe.jpg" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists img-no-padding"></div>
                            <div>
                                <span class="btn btn-outline-default btn-file btn-round"><span class="fileinput-new">Change Photo</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                                <br>
                                <a href="#paper-kit" class="btn btn-link btn-danger fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <form class="settings-form">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control border-input" placeholder="First Name">
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Last Email</label>
                                        <input type="text" class="form-control border-input" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                          <div class="form-group">
                                <label>Job Title</label>
                                <input type="text" class="form-control border-input" placeholder="Job Title">
                          </div>
                          <div class="form-group">
                            <label>Description</label>
                                <textarea class="form-control textarea-limited" placeholder="This is a textarea limited to 150 characters." rows="3" ,="" maxlength="150"></textarea>
                                <h5><small><span id="textarea-limited-message" class="pull-right">150 characters left</span></small></h5>
                          </div>

                        <label>Notifications</label>
                        <ul class="notifications">
                              <li class="notification-item">
                                Updates regarding platform changes
                                <div class="checkbox pull-right">
                                <input id="checkbox1" type="checkbox">
                                <label for="checkbox1">
                                    Agree
                                </label>
                                </div>
                              </li>
                              <li class="notification-item">
                                Updates regarding product changes
                                   <div class="checkbox pull-right">
                                <input id="checkbox2" type="checkbox">
                                <label for="checkbox2">
                                    Agree
                                </label>
                                </div>
                              </li>
                              <li class="notification-item">
                                Weekly newsletter
                                   <div class="checkbox pull-right">
                                <input id="checkbox3" type="checkbox">
                                <label for="checkbox3">
                                    Agree
                                </label>
                                </div>
                              </li>
                          </ul>
                          <div class="text-center">
                            <button type="submit" class="btn btn-wd btn-info btn-round">Guardar</button>
                          </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
	
    @include('partials.footer')

    </body>




@endsection