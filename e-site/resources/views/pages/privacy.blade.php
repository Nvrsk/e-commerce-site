@extends ('layout')


@section('body')

<body>



    @include('partials.navbar')


    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('assets/img/login-image.jpg');">
         <div class="filter"></div>
     </div>
     <div class="main">
        <div class="section profile-content">
            <div class="container">
                <br>
                <div class="owner">
                 <div class="icon icon-danger mb-3">
                     <h2><i class="nc-icon nc-single-copy-04 "></i></h2>
                 </div>
                 <div class="name">
                    <h4 class="title">Política de la Compañia<br /></h4>
                    <h6 class="description">Privacidad</h6>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center tagline">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio eligendi ratione quam optio expedita deserunt.</p>
                    <br />
                    
                </div>
                
            </div>
            <div class="separator">■</div>

            <h6>Privacy Policy</h6>

            <p>Last updated: August 25, 2017</p>

            <p>E-COMMERCE ("us", "we", or "our") operates the www.e-commerce.com website (the "Service").</p>

            <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>

            <p>We will not use or share your information with anyone except as described in this Privacy Policy. This Privacy Policy is licensed by <a href="https://termsfeed.com" rel="nofollow">TermsFeed Generator</a> to E-COMMERCE.</p>

            <p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at www.e-commerce.com</p>


            <h6>Information Collection And Use</h6>

            <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name, phone number, postal address, other information ("Personal Information").</p>

            <h6>Log Data</h6>

            <p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>

            <h6>Cookies</h6>

            <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.</p>

            <p>We use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>

            <h6>Service Providers</h6>

            <p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>

            <p>These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

            <h6>Security</h6>

            <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

            <h6>Links To Other Sites</h6>

            <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>

            <p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>

            <h6>Children's Privacy</h6>

            <p>Our Service does not address anyone under the age of 13 ("Children").</p>

            <p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you are aware that your Children has provided us with Personal Information, please contact us. If we discover that a Children under 13 has provided us with Personal Information, we will delete such information from our servers immediately.</p>

            <h6>Changes To This Privacy Policy</h6>

            <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>

            <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

            <h6>Contact Us</h6>

            <p>If you have any questions about this Privacy Policy, please contact us.</p>
        </div>
    </div>
</div>



@include('partials.footer')

</body>




@endsection