@extends ('layout')


@section('body')
<body class="blog-page">

    @include('partials.navbar')

    <div class="page-header" data-parallax="true" style="background-image: url(assets/img/cover.jpg); transform: translate3d(0px, 0px, 0px);">
        <div class="filter"></div>
        <div class="content-center">
            <div class="title-brand">
                <h6 class="presentation-title text-center">{{$products->title}}</h6>
                <h3 class="text-center">Make your mark with a new design.</h3>
                <br>
                <a href="#pablo" class="btn btn-danger btn-round btn-lg">
                    <i class="fa fa-share-alt" aria-hidden="true"></i> Share Article
                </a>
            </div>
        </div>
    </div>


    <div class="wrapper">
        <div class="main">
            <div class="section section-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 text-center title mb-0">
                            <h2>A place for creating</h2>
                            <h3 class="title-uppercase"><small>Made by designers for snowboarders</small></h3>
                        </div>
                    </div>
                    <div class="article">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="text-center">
                                    <span class="label label-danger main-tag">Briefing</span>
                                    <a href="javascrip: void(0);"><h3 class="title">Make Somebody Happy</h3></a>
                                    <h6 class="title-uppercase">Last Update:  10-2016</h6>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <a href="javascrip: void(0);">
                                    <div class="card" data-radius="rounded" style="background-image: url('assets/img/{{$products->img}}.jpg');"></div>
                                    <p class="image-thumb text-center">Online Screenshot</p>
                                </a>
                                <div class="article-content">
                                    <h4>Follow unconventional beliefs</h4>
                                    <p>
                                       You won’t find many concepts that are very useful or important if you insist on having a worldview that’s void of controversy, invulnerable to criticism, and incapable of making others feel confused.
                                   </p>

                                   <p>
                                    Interesting ideas are a reward for not being afraid to have unconventional beliefs.
                                    You can’t grow if you’re never willing to turn your back on the status quo. You can’t expand if you’re never willing to take an unorthodox stand. You can’t have a beautiful mind if you’re never willing to leave the crowd behind.
                                </p>
                                <blockquote class="blockquote">
                                  <p>"Don’t settle: Don’t finish crappy books. If you don’t like the menu, leave the restaurant. If you’re not on the right path, get off it."</p>
                                  <footer>- Chris Brogan in <cite title="Source Title">Trust Agents</cite></footer>
                              </blockquote>
                              <p>
                                It’s easier to fear rejection than it is to open our minds to something new, but doing what’s easy doesn’t always equal doing what’s authentic, enriching, and meaningful.
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascrip: void(0);">
                                        <div class="card" data-radius="rounded" style="background-image: url('assets/img/app_1.jpg'); background-position: center;">
                                        </div> <!-- end card -->
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascrip: void(0);">
                                        <div class="card" data-radius="rounded" style="background-image: url('assets/img/app_2.jpg'); background-position: center;"></div>
                                    </a>
                                </div>
                            </div>
                            <p>

                            </p>
                            <h4>Ideas Worth Mentioning</h4>
                            <p>The stories, ideas and lessons are enough to fill a year’s worth of articles, but for now I wanted to share the ideas straight from the people creating the disruption. Below are my most impactful takeaways from the last few days: </p>
                            <p><strong>No one belongs here more than me.</strong> When in doubt of your surroundings, this is the mantra.</p>
                            <p><strong>The ultimate currency is being uncool.</strong> Be vulnerably you and watch how you connect.</p>
                            <p><strong>The opposite of scarcity is enough.</strong> Be confident that if you’re doing work that matters to you, you are enough. There is no comparison.</p>
                            <p><strong>Unused creativity is not benign</strong> – it turns into grief. Do something with it.</p>
                            <p><strong>Get in the arena</strong>, show up, do your thing and don’t be afraid to get your ass kicked a little bit.</p>
                            <p>Who you are will always trump who you think people want you to be.</p>
                            <p><strong>You can’t control if someone loves you back.</strong> Love them anyway.</p>

                            <h4><strong>Conclusions</strong></h4>
                            <p>If all of your convictions can be expressed in a sound bite on mainstream television without provoking the slightest bit of anger or annoyance in anyone whatsoever, I think it’s safe to say that your outlook on life offers you very few opportunities for the remarkable.
                            </p>
                        </div>
                        
                        <hr>

                        
                    </div>
                </div>
            </div>
            <div class="related-articles">
                <h3 class="title">Related Products</h3>
                <legend></legend>
                <div class="row">
                    <div class="col-md-3">
                        <a href="pkp"><img src="assets/img/project_2.jpg" alt="Rounded Image" class="img-rounded img-responsive"></a>
                        <p class="blog-title">Light Tree Dashboard</p>
                    </div>
                    <div class="col-md-3">
                        <a href="pkp"><img src="assets/img/project_5.jpg" alt="Rounded Image" class="img-rounded img-responsive"></a>
                        <p class="blog-title">Kitesurf Image Cloud</p>
                    </div>
                    <div class="col-md-3">
                        <a href="pkp"><img src="assets/img/project_3.jpg" alt="Rounded Image" class="img-rounded img-responsive"></a>
                        <p class="blog-title">Design Kit Pro</p>
                    </div>
                    <div class="col-md-3">
                        <a href="pkp"><img src="assets/img/project_4.jpg" alt="Rounded Image" class="img-rounded img-responsive"></a>
                        <p class="blog-title">Evergreen Web Kit</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


@include('partials.footer')

</body>




@endsection
