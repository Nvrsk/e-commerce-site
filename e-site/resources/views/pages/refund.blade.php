@extends ('layout')
    

@section('body')

    <body>



    @include('partials.navbar')



    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('assets/img/login-image.jpg');">
			<div class="filter"></div>
		</div>
<div class="main">
            <div class="section profile-content">
            <div class="container">
            <br>
                <div class="owner">
                   <div class="icon icon-danger mb-3">
                       <h2><i class="nc-icon nc-user-run"></i></h2>
                   </div>
                    <div class="name">
                        <h4 class="title">Devoluciones<br /></h4>
                        <h6 class="description">Condiciones</h6>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center tagline">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio eligendi ratione quam optio expedita deserunt.</p>
                        <br />
                       
                    </div>
                         
                </div>
                    <div class="separator">■</div>

                   


<p>Thank you for shopping at E-COMMERCE.</p>

<p>Please read this policy carefully. This is the Return and Refund Policy of E-COMMERCE. This Return and Refund Policy is licensed by <a href="https://termsfeed.com" rel="nofollow">TermsFeed Generator</a> to E-COMMERCE.</p>




<h6>Digital products</h6>

<p>We do not issue refunds for digital products once the order is confirmed and the product is sent.</p>

<p>We recommend contacting us for assistance if you experience any issues receiving or downloading our products.</p>



<h6>Contact us</h6>

<p>If you have any questions about our Returns and Refunds Policy, please contact us:</p>

<ul>
<li>
    <p>By visiting this page on our website: www.e-commerce.com/refund</p>
</li>
</ul>


</div>
</div>
    <!-- modal   -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.
            </div>
            <div class="modal-footer">
                <div class="left-side">
                    <button type="button" class="btn btn-default btn-link" data-dismiss="modal">Never mind</button>
                </div>
                <div class="divider"></div>
                <div class="right-side">
                    <button type="button" class="btn btn-danger btn-link">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- end modal -->

    
    @include('partials.footer')

    </body>




@endsection