@extends ('layout')


@section('body')
<body>



    @include('partials.navbar')
    
    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('assets/img/login-image.jpg');">
         <div class="filter"></div>
     </div>
     <div class="main">
        <div class="section profile-content">
            <div class="container">
                <br>
                <div class="owner">
                 <div class="icon icon-danger mb-3">
                     <h2><i class="nc-icon nc-album-2"></i></h2>
                 </div>
                 <div class="name">
                    <h4 class="title">Galería<br /></h4>
                    <h6 class="description">Nuestras Imágenes</h6>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center tagline">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio eligendi ratione quam optio expedita deserunt.</p>
                    <br />
                    
                </div>
                
            </div>
            <div class="separator">■</div>


            

            <div class="row text-center text-lg-left">

               
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300"  class="img-fluid img-thumbnail"></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>
                <div class="col-md-3">
                    <div class="card"><a href="#" data-toggle="modal" data-target="#noticeModal" class="d-block mb-4 h-100"><img src="http://placehold.it/400x300" alt="" class="img-fluid img-thumbnail"></a></div>
                </div>

                
            </div>

            
        </div>  
    </div>

</div>
</div>
<!-- modal   -->
@include('partials.simple-modal')
<!-- end modal -->

@include('partials.footer')

</body>




@endsection

