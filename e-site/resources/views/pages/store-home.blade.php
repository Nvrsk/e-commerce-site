			@extends ('layout')
			

			@section('body')

			<body>



				@include('partials.navbar')



				<div class="wrapper">
					        <div class="page-header page-header-xs" style="background-image: url(assets/img/cover.jpg);min-height: 18vh !important;">
          <div class="filter"></div>
          <div class="content-center mt-0">
            <div class="container">
              <div class="title-brand">
                <div class="presentation-title">
                 
                  


              </div>


          </div>

      </div>
  </div>
</div>
					<!-- header -->
		<div class="container">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="row">
									<div class="col-md-4">
										<div class="card card-product card-plain" data-toggle="tooltip" data-placement="top" title="Click to redirect to single product page">
											<div class="card-image" >
												<a href="{{ URL::route('store-product') }}">
													<img src="assets/img/jacket (11).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>
												<div class="card-block">
													<div class="card-description">
														<h5 class="card-title">Zine Camo Windbreaker</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>3.915 €</strike> <span class="text-danger">3.125 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card card-product card-plain">
											<div class="card-image">
												<a href="store-product.html">
													<img src="assets/img/jacket (1).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>
												<div class="card-block">
													<div class="card-description">
														<h5 class="card-title">Hillstomper Hiking Coat</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>2.675 €</strike> <span class="text-danger">2.000 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card card-product card-plain">
											<div class="card-image">
												<a href="store-product.html">
													<img src="assets/img/jacket (12).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>					
												<div class="card-block">									
													<div class="card-description">
														<h5 class="card-title">Grey Velvet Windbreaker</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>3.520 €</strike> <span class="text-danger">2.900 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>
	</div>
    </div>
    <div class="carousel-item">
       <div class="row">
       							
									<div class="col-md-4">
										<div class="card card-product card-plain" data-toggle="tooltip" data-placement="top" title="Click to redirect to single product page">
											<div class="card-image" >
												<a href="{{ URL::route('store-product') }}">
													<img src="assets/img/jacket (9).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>
												<div class="card-block">
													<div class="card-description">
														<h5 class="card-title">Zine Camo Windbreaker</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>3.915 €</strike> <span class="text-danger">3.125 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="card card-product card-plain">
											<div class="card-image">
												<a href="store-product.html">
													<img src="assets/img/jacket (5).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>
												<div class="card-block">
													<div class="card-description">
														<h5 class="card-title">Hillstomper Hiking Coat</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>2.675 €</strike> <span class="text-danger">2.000 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card card-product card-plain">
											<div class="card-image">
												<a href="store-product.html">
													<img src="assets/img/jacket (7).jpg" alt="Rounded Image" class="img-rounded img-responsive">
												</a>					
												<div class="card-block">									
													<div class="card-description">
														<h5 class="card-title">Grey Velvet Windbreaker</h5>
														<p class="card-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
													</div>
													<div class="price">
														<strike>3.520 €</strike> <span class="text-danger">2.900 €</span>
													</div>
												</div>
											</div>
										</div>
									</div>
	</div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
		</div>
					<!-- end header -->

					<div class="main">
					

						<div class="section section-white">
							<div class="container">
								<h3 class="section-title text-center mb-5" style="text-transform: uppercase;">Encuentra lo que necesitas</h3>
								<hr>
								<div class="row">
									<div class="col-md-4">
										<div class="card card-plain">
											<div class="card-body">
												<h4 class="card-title">Departamentos</h4>
												<p class="card-text">Selecciona una categoría</p>
											</div>
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<button type="button" class="btn btn-round btn-danger category-button" data-filter="all"><h6>Ver Todo</h6></button>
												</li>
												<li class="list-group-item">
													<button type="button" class="btn btn-round btn-danger category-button" data-filter="Jackets"><h6>Ropa</h6></button>
												</li>
												<li class="list-group-item">
													<button type="button" class="btn btn-round btn-danger category-button" data-filter="Shoes"><h6>Zapatos</h6></button>
												</li>
											</ul>
											<hr>
											<p>Filtrar por género</p>
											<div class="card-body">
												
												<div class="nav-tabs-navigation">
													<div class="nav-tabs-wrapper">
														<ul class="nav nav-tabs" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" data-toggle="tab" href="#all" role="tab"><h6>general</h6></a>
															</li>
															<li class="nav-item">
																<a class="nav-link" data-toggle="tab" href="#men" role="tab"><h6>Hombres</h6></a>
															</li>
															<li class="nav-item">
																<a class="nav-link" data-toggle="tab" href="#women" role="tab"><h6>Mujeres</h6></a>
															</li>
														</ul>
													</div>
												</div>
											</div>	
											<hr>
											<p>Busqueda Avanzada</p>

											<form class="form-inline">
												<input class="form-control" type="text" placeholder="Search" aria-label="Search">
												<button class="btn btn-danger my-2 my-sm-0" type="submit"> Buscar</button>
											</form>
										</div>
										<div class="row text-center">
										</div>
									</div>
									
									<div class="col-md-8">
										
										<div class="tab-content following">
											<div class="tab-pane text-center active" id="all" role="tabpanel">
												<div class="row">
													@foreach ($shop_cards as $cards)
													<div class="col-md-3 col-sm-3 all {{$cards->class}}" style="border:solid #ccc 1px;">
														<div class="card card-product card-plain">
															<div class="card-image">
																<a href="#" data-toggle="modal" data-target="#noticeModal">
																	<img src="assets/img/{{$cards->thumb}}.jpg" alt="Rounded Image" class="img-rounded img-responsive">
																</a>
																<div class="card-block">
																	<div class="card-description">
																		<h5 class="card-title">{{$cards->name}}</h5>
																		<p class="card-description">{{$cards->class}}</p>
																	</div>
																	<div class="price">
																		<h5>{{$cards->price}} €</h5>
																	</div>
																</div>
															</div>
														</div>
													</div>
													@endforeach
												</div>
											</div>
											<div class="tab-pane text-center" id="men" role="tabpanel">
												<div class="row">
													@foreach ($men_cards as $cards)
													<div class="col-md-3 col-sm-3 all {{$cards->class}}" style="border:solid #ccc 1px;">
														<div class="card card-product card-plain">
															<div class="card-image">
																<a href="#" data-toggle="modal" data-target="#noticeModal">
																	<img src="assets/img/{{$cards->thumb}}.jpg" alt="Rounded Image" class="img-rounded img-responsive">
																</a>
																<div class="card-block">
																	<div class="card-description">
																		<h5 class="card-title">{{$cards->name}}</h5>
																		<p class="card-description">{{$cards->class}}</p>
																	</div>
																	<div class="price">
																		<h5>{{$cards->price}} €</h5>
																	</div>
																</div>
															</div>
														</div>
													</div>
													@endforeach
												</div>
											</div>
											<div class="tab-pane text-center" id="women" role="tabpanel">
												<div class="row">
													@foreach ($women_cards as $cards)
													<div class="col-md-3 col-sm-3 all {{$cards->class}}" style="border:solid #ccc 1px;">
														<div class="card card-product card-plain">
															<div class="card-image">
																<a href="#" data-toggle="modal" data-target="#noticeModal">
																	<img src="assets/img/{{$cards->thumb}}.jpg" alt="Rounded Image" class="img-rounded img-responsive">
																</a>
																<div class="card-block">
																	<div class="card-description">
																		<h5 class="card-title">{{$cards->name}}</h5>
																		<p class="card-description">{{$cards->class}}</p>
																	</div>
																	<div class="price">
																		<h5>{{$cards->price}} €</h5>
																	</div>
																</div>
															</div>
														</div>
													</div>
													@endforeach
												</div>
											</div>

										</div>
										<div class="row pt-5">
											<div class="col-md-6 offset-md-5">
												<button rel="tooltip" title="This is a morphing button" class="btn btn-round btn-danger" id="successBtn" data-toggle="morphing" data-rotation-color="gray">Cargar más...</button>
											</div>
										</div>
									</div>
									
								</div>
								<hr>
								
							</div>
						</div>
						<!-- section -->
						
						<br>	
						<div class="owner">
							<h2> <div class="icon icon-danger">
								<i class="pe-7s-news-paper"></i>
							</div></h2>
							<div class="name">
								<span class="label label-danger">Notícias Recientes</span>
								<hr>
								<h6 class="description">Check what's on trend</h6>
							</div>
						</div>

						<div class="section section-blog">	
							<div class="container">

								<div class="row">
									<div class="col-md-4">
										<div class="card card-blog">
											<div class="card-image">
												<a href="#">
													<img class="img img-raised" src="assets/img/hiker.jpg">
												</a>
											</div>
											<div class="card-block">
												<h6 class="card-category text-info"><i class="fa fa-compass"></i>Adventure</h6>
												<h5 class="card-title">
													<a href="#">Reaching the peak.</a>
												</h5>
												<p class="card-description">
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, autem, suscipit? Sapiente quo, architecto minima similique... <br>
												</p>
												<hr>
												<div class="card-footer">
													<div class="author">
														<a href="#">
															<img src="assets/img/faces/ayo-ogunseinde-2.jpg" alt="..." class="avatar img-raised">
															<span>Mike John</span>
														</a>
													</div>
													<div class="stats">
														<i class="fa fa-clock-o" aria-hidden="true"></i> 5 min read
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="card card-blog">
											<div class="card-image">
												<a href="#">
													<img class="img img-raised" src="assets/img/climbing__9_.jpg">
												</a>
											</div>
											<div class="card-block">
												<h6 class="card-category text-success"><i class="fa fa-tree"></i>
													Startups
												</h6>
												<h5 class="card-title">
													<a href="#">Staying out in the wild</a>
												</h5>
												<p class="card-description">
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus fugit sint hic in exercitationem repellat!...<br>
												</p>
												<hr>
												<div class="card-footer">
													<div class="author">
														<a href="#">
															<img src="assets/img/faces/kaci-baum-2.jpg" alt="..." class="avatar img-raised">
															<span>Nickie Kelly</span>
														</a>
													</div>
													<div class="stats">
														<i class="fa fa-clock-o" aria-hidden="true"></i> 5 min read
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="card card-blog">
											<div class="card-image">
												<a href="#">
													<img class="img img-raised" src="assets/img/wear.jpg">
												</a>
											</div>

											<div class="card-block">
												<h6 class="card-category text-danger">
													<i class="fa fa-free-code-camp" aria-hidden="true"></i> Enterprise
												</h6>
												<h5 class="card-title">
													<a href="#">City outdoors wear</a>
												</h5>
												<p class="card-description">
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime accusantium dolorem consectetur dicta... <br>
												</p>
												<hr>
												<div class="card-footer">
													<div class="author">
														<a href="#">
															<img src="assets/img/faces/erik-lucatero-2.jpg" alt="..." class="avatar img-raised">
															<span>Mike John</span>
														</a>
													</div>
													<div class="stats">
														<i class="fa fa-clock-o" aria-hidden="true"></i> 5 min read
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- section -->
						
					</div>




				</div> <!-- wrapper -->

				<!-- modals -->


				@include('partials.demo-modal')

				@include('partials.simple-modal')

				

				<!-- end modals -->


				
				@include('partials.footer')

			</body>




			@endsection