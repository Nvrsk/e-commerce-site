@extends ('layout')


@section('body')
<body>



  @include('partials.navbar')


  <div class="wrapper">
    <div class="page-header page-header-xs" style="background-image: url(assets/img/cover.jpg);">
      <div class="filter"></div>
      <div class="content-center mt-0">
        <div class="container">
          <div class="title-brand">
            <div class="presentation-title">
              <h4>GRACIAS POR SU COMPRA!</h4>
              


            </div>


          </div>

        </div>
      </div>
    </div>
    <div class="main">
      <div class="section section-white">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-8 offset-md-2">
              <div class="label label-danger">
                checkout
              </div>
            </div>
          </div>
          <hr>
          <br>
          <div class="row">
            <div class="col-md-6">

              <div class="btn btn-sm btn-danger btn-warning" data-toggle="collapse" data-target="#promo"><i class="nc-icon nc-money-coins pt-2"></i></div>

              <label data-toggle="collapse" data-target="#promo">
                Tengo un código promocional
              </label>



              <div class="collapse show" aria-expanded="true" id="promo">
                <div class="form-group">
                 <br>
                 <div class="form-inline">
                  <input type="text" class="form-control pl-2" id="inputpromo" placeholder="Enter promo code">
                  <button class="btn btn-sm  ml-2 btn btn-primary"> Aplicar</button>
                </div>
              </div>
            </div>
            <hr>
            <div class="row text-center mb-3">
              <div class="col md-2">
               
                <label for=""><h6>Enviar mi pedido a</h6></label><h4 class="mt-0"> <i class="nc-icon nc-pin-3"></i></h4>
                
              </div>
            </div>

            <div class="list-group" style="border: 1px #ccc solid;">
              <div class="list-group-item">
                <div class="list-group-item-heading">          
                  <div class="row radio">

                    <div class="col-md-6">
                     <div class="radio">
                      <input type="radio" name="radio1" id="radio1" value="option1">
                      <label for="radio1">
                        1509 Latona St
                      </label>
                    </div>
                    <div class="row">
                      <div class="col-md-2">                  <button class="btn btn-sm btn-danger">Edit</button>
                      </div>
                      <div class="col-md-6">                  <button class="btn btn-sm  btn-danger btn-link">Eliminar esta dirección</button>
                      </div>
                    </div>

                  </div>
                  <div class="col-md-6">
                    <dl class="dl-small">
                      <dt>Miguel Perez</dt>
                      <dd>1509 Latona St, Philadelphia, PA 19146 </dd>
                    </dl>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
           <div class="col-md-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#noticeModal">
              Agregar nueva dirección
            </button>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <form role="form" >
             <div class="btn btn-sm btn-danger btn-warning" data-toggle="collapse" data-target="#gift"><i class="nc-icon nc-paper pt-1"></i></div>
             <label >
               Me gustaría incluir un mensaje
             </label>

             <div class="form-group collapse show" id="gift">
              <label for="inputGift" class="control-label">Gift Message</label>
              <textarea class="form-control form-control-large" rows="3"></textarea>
              <p class="help-block">256 characters left</p>
            </div>
          </form>
        </div>
      </div>
      <hr>
      <div class="row text-center mb-4">
        <div class="col md-2">
          <h6>Métodos de pago</h6>
          <h3 class="mt-0 mb-1 text-center"> <i class="nc-icon nc-credit-card"></i></h3>
        </div>
      </div>




      <div class="row">
        <div class="cold-md-12">
          <div class="list-group" style="border:1px #ccc solid;">
            <div class="list-group-item">
              <div class="list-group-item-heading">          
                <div class="row radio">
                  <div class="col-md-4">
                    <div class="radio">
                      <input type="radio" name="radio2" id="radio3" value="option3" checked="">
                      <label for="radio3">
                        My Visa Card
                      </label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-3">                      
                        <dl class="dl-small">
                          <dt>Credit Card Number</dt>
                          <dd>**********1111</dd>
                        </dl>
                      </div>
                      <div class="col-md-2">
                        <dl class="dl-small">
                          <dt>Expiration</dt>
                          <dd>07/2016</dd>
                        </dl>
                      </div>
                      <div class="col-md-7">
                        <dl class="dl-small">
                          <dt>Billing Address</dt>
                          <dd>1509 Latona St, Philadelphia, PA 19146 </dd>
                        </dl>
                      </div>
                    </div>
                    <button class="btn btn-sm btn-danger">Edit</button>
                    <button class="btn btn-sm btn-danger btn-link">Delete this card</button>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="list-group-item">
              <div class="list-group-item-heading">          
                <div class="row radio">
                  <div class="col-md-7">
                    <div class="radio">
                      <input type="radio" name="radio2" id="radio4" value="option4" checked=""  >
                      <label for="radio4">
                        A New Credit Card
                      </label>
                    </div>

                  </div>
                  <div class="col-md-12">                      
                    <div class="media">
                      <div class="col-md-6">
                        <a class="media-left" href="#">
                          <img src="https://lovewithfood.com/assets/credit_cards/cards-b3a7c7b8345355bf110ebedfd6401312.png" height="25" alt="" />
                        </a>
                      </div>
                      <div class="col-md-6">
                        <div class="media-body" id="newcard">
                          We accept these credit cards.
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <div class="list-group-item-heading">          
                <div class="row radio">
                  <div class="col-md-4">
                    <div class="radio">
                      <input type="radio" name="radio2" id="radio5" value="option5" checked=""  >
                      <label for="radio5">
                        Paypal
                      </label>
                    </div>
                  </div>
                  <div class="col-md-12">                      
                    <div class="media">
                      <div class="col-md-2">
                        <a class="media-left" href="#">
                          <img src="https://www.paypalobjects.com/webstatic/mktg/logo-center/PP_Acceptance_Marks_for_LogoCenter_76x48.png" height="25" alt="" />
                        </a>
                      </div>
                      <div class="col-md-10">
                        <div class="media-body">
                          When you click "Place Order", you will be taken to the PayPal website.
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="col-md-6 text-center">

     <label><h6>Revisa tu orden</h6></label>
     
     
     <h4 class="mt-0 text-center"> <i class="nc-icon nc-zoom-split"></i></h4>

     <br>

     

     <div class="well text-center" style="border:solid 1px #ccc;">
      
      
       <table class="table table-responsive">
        <thead class="thead-inverse">
          <tr>
            <th class="text-center"><i class="nc-icon nc-bullet-list-67"></i></th>
            <th>Artículo</th>
            <th class="text-center">Precio</th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Total</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="img-container">
                <img src="assets/img/jacket (1).jpg" alt="Agenda">
              </div>
            </td>
            <td class="td-product">
              <strong>Crystal Windbreaker</strong>
              
            </td>

            <td class="td-price">
              <small>€149</small>
            </td>
            <td class="td-number td-quantity">
              1
              
            </td>
            <td class="td-number">
              <small>€</small>149
            </td>
          </tr>
          <tr>
           <td>
            <div class="img-container">
              <img src="assets/img/jacket (4).jpg" alt="Stylus">
            </div>
          </td>
          <td class="td-product">
            <strong>Stretch-Knit Jacket</strong>
            
          </td>

          <td class="td-price">
            <small>€2.600</small>
          </td>
          <td class="td-number td-quantity">
            2
            
          </td>
          <td class="td-number">
            <small>€</small>5.200
          </td>
        </tr>
        <tr>
          <td>
            <div class="img-container">
              <img src="assets/img/aditional_product.jpg" alt="Evernote">
            </div>
          </td>
          <td class="td-product">
            <strong>Army Tactical Net Scarf</strong>
          </td>

          <td class="td-price">
            <small>€975 </small>
          </td>
          <td class="td-number td-quantity">
            1
            <div class="btn-group">
             
            </div>
          </td>
          <td class="td-number">
            <small>€</small>975 
          </td>
        </tr>
        <tr>
          <td colspan="2"></td>
          <td></td>
          <td class="td-total">
           Total
         </td>
         <td class="td-total">
          <small>€</small>12,999
        </td>
      </tr>
      <tr class="tr-actions text-center">
       <td colspan="3"></td>
       <td colspan="3">
         <a href="{{ URL::route('store') }}"><button type="button" class="btn btn-primary btn-lg">Añadir + artículos<i class="fa fa-chevron-right"></i></button></a>
       </td>
     </tr>
   </tbody>
 </table>
</div>
<hr>
<h6>Servício de Entrega</h6>
<h3 class="mt-0 mb-1 text-center"> <i class="nc-icon nc-delivery-fast"></i></h3>
<div class="well">
  <div class="container">
    <form class="mt-4">
      

      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-legend col-sm-4">Opciones de envío</legend>
          <div class="col-sm-8 text-left">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                Option one is this and that&mdash;be sure to include why it's great
              </label>
            </div>
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                Option two can be something else and selecting it will deselect option one
              </label>
            </div>
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                Option three can be something else and selecting it will deselect option one
              </label>
            </div>
          </div>
        </div>
      </fieldset>
      <div class="form-group row">

        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <div class="form-check text-left">
            <label class="form-check-label ">
              <input class="form-check-input " type="checkbox"> <h6 class="text-success">Asegurar el paquete</h6>
            </label>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary">Confirmar</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>

</div>
</div>



</div>



<button type="button" class="btn btn-primary btn-lg btn-block" style="border-radius: 0;" data-toggle="modal" data-target="#orderModal">Realizar Compra</button>




</div>




<!-- Modal -->
<div class="modal" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-notice modal-lg pt-1">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title mt-4" id="noticeModal">Ingrese los Datos</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-9">                      
          <form role="form" class="">
            <div class="form-group">
              <label for="inputname">Nombre</label>
              <input type="text" class="form-control form-control-large" id="inputname" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="inputAddress1">Dirección 1</label>
              <input type="text" class="form-control form-control-large" id="inputAddress1" placeholder="Enter address">
            </div>
            <div class="form-group">
              <label for="inputAddress2">Dirección 2</label>
              <input type="text" class="form-control form-control-large" id="inputAddress2" placeholder="Enter address">
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="inputZip">Código ZIP</label>
                  <input type="text" class="form-control form-control-small" id="inputZip" placeholder="Enter zip">
                </div>
              </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label for="inputCity">Ciudad</label>
                  <input type="text" class="form-control" id="inputCity" placeholder="Enter city">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputState" class="control-label">Estado</label>
              <select class="custom-select">
                <option selected>Seleccione un Estado</option>
                <option value="1">Estado 1</option>
                <option value="2">Estado 2</option>
                <option value="3">Estado 3</option>
              </select>
            </div>
          </form>
          <button class="btn btn-sm btn-danger">Guardar Dirección</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title text-success" id="exampleModalLabel"><strong>¡Tu orden se ha enviado!</strong></h6>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, omnis iste voluptas officia rerum perspiciatis, maxime, incidunt similique autem fugiat animi culpa repellat numquam temporibus blanditiis ipsum voluptatibus aliquam cupiditate. 
                                        </div>
                                        <div class="modal-footer">
                                            <div class="left-side">
                                                <button type="button" class="btn btn-default btn-link" data-dismiss="modal" >Cerrar</button>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="right-side">
                                                <button type="button" class="btn btn-danger btn-link" >Comprar nuevamente <i class="nc-icon nc-cart-simple"></i></button>
                                            </div>
                                        </div>
                                    </div>
  </div>
</div>


@include('partials.footer')


</body>




@endsection


