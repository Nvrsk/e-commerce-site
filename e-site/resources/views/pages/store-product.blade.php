@extends ('layout')
    

@section('body')

    <body>



    @include('partials.navbar')

	<div class="wrapper">

	<!-- header -->
		<div class="page-header page-header-xs" style="background-image: url('assets/img/login-image.jpg');">
			<div class="filter"></div>
		</div>

	<!-- end header -->
	<!-- content -->
    <div class="main">
        <div class="section">
            <div class="container">
                    <div class="row title-row">
                        <div class="col-md-6 ">
                            <span class="label label-danger"><strong>SHOP</strong></span>
                        </div>
                        <div class="col-md-6 ">
                            <div class="pull-right">
                                <span class="text-muted">Order Status</span> <a href="{{ URL::route('shopping-cart') }}"><button class="btn btn-link"> <i class="fa fa-shopping-cart"></i> 3 Items</button></a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4 offset-md-2 col-sm-3">
                            <div id="carousel m-0">
								<div class="card page-carousel">
									<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
									    <ol class="carousel-indicators">
										    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
										    <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
											<li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
											<li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
									    </ol>
			                            <div class="carousel-inner" role="listbox">
				                            <div class="carousel-item carousel-item-next carousel-item-left">
				                                <img class="d-block img-fluid" src="assets/img/product_1.jpg" alt="Awesome Item">
				                            	<div class="carousel-caption d-none d-md-block">
				                                    <p>Front</p>
				                                </div>
				                            </div>
                                            
				                            <div class="carousel-item">
				                                <img class="d-block img-fluid" src="assets/img/product_2.jpg" alt="Awesome Item">
				                            	<div class="carousel-caption d-none d-md-block">
				                            	    <p>Rear</p>
				                            	</div>
				                            </div>
				                            <div class="carousel-item">
				                                <img class="d-block img-fluid" src="assets/img/product_3.jpg" alt="Awesome Item">
				                            	<div class="carousel-caption d-none d-md-block">
				                            	    <p>Pocket Detail</p>
				                            	</div>
				                            </div>
											<div class="carousel-item active carousel-item-left">
				                                <img class="d-block img-fluid" src="assets/img/product_4.jpg" alt="Awesome Item">
				                            	<div class="carousel-caption d-none d-md-block">
				                            	    <p>Neck Detail</p>
				                            	</div>
				                            </div>
			                            </div>

			                            <a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			                                <span class="fa fa-angle-left"></span>
			                                <span class="sr-only">Previous</span>
			                            </a>
			                            <a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			                                <span class="fa fa-angle-right"></span>
			                                <span class="sr-only">Next</span>
			                            </a>
									</div>
								</div>
                            </div> <!-- end carousel -->

                        </div>
                        <div class="col-md-5 col-sm-6">
							<h2>Zine Camo Windbreaker</h2>
							<h4 class="price"><strong>€ 2,900.00</strong></h4>
							<hr>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae deleniti ducimus placeat</p>
							<span class="label label-default shipping">Free shipping to Europe </span>

                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label>Select color</label>
                                    <div class="form-group">
                                        <div class="btn-group bootstrap-select"><button type="button" class="dropdown-toggle btn btn-outline-default btn-block btn-round" data-toggle="dropdown" role="button" title="Black"><span class="filter-option pull-left">Black </span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">Black </span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Gray</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">White</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select name="huge" class="selectpicker" data-style="btn btn-outline-default btn-block btn-round" data-menu-style="" tabindex="-98">
                                            <option value="1">Black </option>
                                            <option value="2">Gray</option>
											<option value="3">White</option>
                                        </select></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Select size</label>
                                    <div class="form-group">
                                        <div class="btn-group bootstrap-select"><button type="button" class="dropdown-toggle btn btn-outline-default btn-block btn-round" data-toggle="dropdown" role="button" title="Small"><span class="filter-option pull-left">Small </span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">Small </span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Medium</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Large</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select name="huge" class="selectpicker" data-style="btn btn-outline-default btn-block btn-round" data-menu-style="" tabindex="-98">
                                            <option value="1">Small </option>
                                            <option value="2">Medium</option>
                                            <option value="3">Large</option>

                                        </select></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-7 offset-md-5 col-sm-8">
                                    <button class="btn btn-danger btn-block btn-round">Add to Cart &nbsp;<i class="fa fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

        <div class="section">
            <div class="container">
                <div class="row card-block-row">
                    <div class="col-md-4 col-sm-4">
                       <div class="info">
                            <div class="icon icon-warning">
                                <i class="nc-icon nc-delivery-fast"></i>
                            </div>
                            <div class="description">
                                <h4 class="info-title"> 2 Days Delivery </h4>
                                <p>Spend your time generating new ideas. You don't have to think of implementing anymore.</p>
                            </div>
                       </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                       <div class="info">
                            <div class="icon icon-danger">
                                <i class="nc-icon nc-credit-card"></i>
                            </div>
                            <div class="description">
                                <h4 class="info-title"> Refundable Policy </h4>
                                <p>Larger, yet dramatically thinner. More powerful, but remarkably power efficient.</p>
                            </div>
                       </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                       <div class="info">
                            <div class="icon icon-success">
                                <i class="nc-icon nc-bulb-63"></i>
                            </div>
                            <div class="description">
                                <h4 class="info-title"> Popular Item </h4>
                                <p>Choose from a veriety of colors resembling sugar paper pastels.</p>
                            </div>
                       </div>
                    </div>
                </div>
                <hr>
                    <p>What to find out more about how we build our stuff? <a class="link-danger">Learn more.</a></p>
                <hr>
                <div class="faq">

                    <h4>Frequently Asked Questions</h4> <br>
					<div id="acordeon">
						<div id="accordion" role="tablist" aria-multiselectable="true">
							<div class="card no-transition">
								<div class="card-header card-collapse" role="tab" id="headingOne">
									<h5 class="mb-0 panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
											Default Collapsible Item 1
											<i class="nc-icon nc-minimal-down"></i>
										</a>
									</h5>
								</div>
								<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
									<div class="card-block">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
									</div>
								</div>
								<div class="card-header card-collapse" role="tab" id="headingTwo">
									<h5 class="mb-0 panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											Default Collapsible Item 2
											<i class="nc-icon nc-minimal-down"></i>
										</a>
									</h5>
								</div>
								<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="card-block">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
									</div>
								</div>
								<div class="card-header card-collapse" role="tab" id="headingThree">
									<h5 class="mb-0 panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											Default Collapsible Item 3
											<i class="nc-icon nc-minimal-down"></i>
										</a>
									</h5>
								</div>
								<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="card-block">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
									</div>
								</div>
							</div>
						</div><!--  end acordeon -->
					</div>

                </div>
                <div class="row add-row">
                    <div class="col-md-4 col-sm-4">
                        <h4>Complete the Look</h4>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <h5>Army Tactical Net Scarf</h5>
                        <p class="price"><strong>€ 975.00</strong></p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam similique accusamus soluta.</p>
						<br>
                        <button class="btn btn-danger btn-round"> Add to Cart</button>
                    </div>
                    <div class="col-md-4  col-sm-4">
                        <img src="assets/img/aditional_product.jpg" alt="Rounded Image" class="img-rounded img-fluid mt-3">
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><h4>Similar Products</h4><br></div>
                    <div class="col-md-4 col-sm-4">
						<div class="card card-product card-plain">
							<div class="card-image">
								<a href="#" data-toggle="modal" data-target="#noticeModal">
									<img src="assets/img/jacket (1).jpg" alt="Rounded Image" class="img-rounded img-responsive">
								</a>
								<div class="card-block">
									<div class="card-description">
										<h5 class="card-title">Grey Velvet Windbreaker</h5>
										<p class="card-description">Jackets</p>
									</div>
									<div class="actions">
										<h5>1.300 €</h5>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-4 col-sm-4">
						<div class="card card-product card-plain">
							<div class="card-image">
								<a href="#" data-toggle="modal" data-target="#noticeModal">
									<img src="assets/img/jacket (3).jpg" alt="Rounded Image" class="img-rounded img-responsive">
								</a>
								<div class="card-block">
									<div class="card-description">
										<h5 class="card-title">Foggy Day Raincoat Jacket</h5>
										<p class="card-description">Jackets</p>
									</div>
									<div class="actions">
										<h5>1.500 €</h5>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-4 col-sm-4">
						<div class="card card-product card-plain">
	                        <div class="card-image">
	                            <a href="#" data-toggle="modal" data-target="#noticeModal">
	                                <img src="assets/img/jacket (6).jpg" alt="Rounded Image" class="img-rounded img-responsive">
	                            </a>
	                            <div class="card-block">
	                                <div class="card-description">
	                                    <h5 class="card-title">Purple Orchid Climbing Coat</h5>
	                                    <p class="card-description">Only on demand</p>
	                                </div>

	                                <div class="actions">
	                                    <h5>1.200 €</h5>
	                                </div>
	                            </div>
	                        </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    	<!-- end content -->
</div>


<div class="modal" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                                <div class="modal-dialog modal-notice pt-1">
                                    <div class="modal-content">
                                        <div class="modal-header no-border-header">
                                            <h5 class="modal-title" id="myModalLabel">Item Detailed Info </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="instruction">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p> <strong><h6 class="title">1. Front </h6></strong>  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur perferendis reprehenderit iure sed.</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="picture">
                                                            <img src="https://placehold.it/200x200?text=Front" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="instruction">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p> <strong><h6 class="title">2. Rear </h6></strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum hic amet officiis id ducimus eius nulla adipisci libero debitis.</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="picture">
                                                            <img src="https://placehold.it/200x200?text=Rear" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary btn-link" data-dismiss="modal"><i class="nc-icon nc-cart-simple"></i><h6>Agregar al carrito</h6></button>
                                        </div>
                                    </div>
                                </div>
</div>
	
	


    <!-- modals -->


    @include('partials.demo-modal')

    @include('partials.simple-modal')

    

    <!-- end modals -->


    
    @include('partials.footer')

    </body>




@endsection