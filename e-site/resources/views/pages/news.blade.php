@extends ('layout')


@section('body')

<body class="blog">



    @include('partials.navbar')


    <div class="wrapper">
        <div class="page-header page-header-xs" style="background-image: url(assets/img/cover.jpg);">
          <div class="filter"></div>
          <div class="content-center mt-0">
            <div class="container">
              <div class="title-brand">
                <div class="presentation-title">
                  <h4>MANTENTE AL DIA</h4>
                  


              </div>


          </div>

      </div>
  </div>
</div>
<div class="main">
    <div class="section section-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center title">
                    <h2>A place for storytelling</h2>
                    <h3 class="title-uppercase"><small>Written by designers for designers</small></h3>
                </div>
            </div>
            <div class="article">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="card card-blog card-plain text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="img img-raised" src="assets/img/hiker.jpg">
                                </a>
                            </div>
                            <div class="card-block">
                                <div class="card-category">
                                    <span class="label label-primary main-tag">Featured</span>
                                </div>
                                <a href="javascrip: void(0);">
                                    <h3 class="card-title">Reaching The Peak</h3>
                                </a>
                                <div class="card-description">
                                    <p>In the first sentence of Pitchfork’s review of my collaborative project with 9th Wonder, INDIE 500, a reviewer who is associated with music review site rhapsody.com writes about how I criticize and then distance myself from “celebrity straw men” with the line “celebrities be making money...</p>
                                </div>
                            </div>
                            <button class="btn btn-danger btn-round btn-sm">Read more</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="article">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="card card-blog card-plain text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="img img-raised" src="assets/img/night_view.jpg">
                                </a>
                            </div>
                            <div class="card-block">
                                <div class="card-category">
                                    <span class="label label-info main-tag">Popular</span>
                                </div>
                                <a href="javascrip: void(0);">
                                    <h3 class="card-title">Staying out in the wild</h3>
                                    <h6 class="title-uppercase">October 20, 2016</h6>
                                </a>
                                <div class="card-description">
                                    <p>In the first sentence of Pitchfork’s review of my collaborative project with 9th Wonder, INDIE 500, a reviewer who is associated with music review site rhapsody.com writes about how I criticize and then distance myself from “celebrity straw men” with the line “celebrities be making money...</p>
                                </div>
                            </div>
                            <button class="btn btn-danger btn-round btn-sm">Read more</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="article">
                <div class="col-md-8 offset-md-2">
                    <div class="card card-blog card-plain text-center">
                        <div class="card-image">
                            <a href="#pablo">
                                <img class="img img-raised" src="assets/img/wear_leaf.jpg">
                                <p class="image-thumb">Photo by Cam Adams</p>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="card-category">
                                <span class="label label-warning main-tag">Trending</span>
                            </div>
                            <a href="javascrip: void(0);">
                                <h3 class="card-title">City outdoors wear</h3>
                                <h6 class="title-uppercase">October 20, 2016</h6>
                            </a>
                            <div class="card-description">
                                <p>You won’t find many concepts that are very useful or important if you insist on having a worldview that’s void of controversy, invulnerable to criticism, and incapable of making others feel confused...</p>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-round btn-sm">Read more</button>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row">
                <div class="col-md-2 offset-md-10">
                    <div class="pull-right">
                        <button class="btn btn-link btn-default btn-move-right">Older Posts <i class="fa fa-angle-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


@include('partials.footer')

</body>




@endsection