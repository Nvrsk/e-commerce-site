@extends ('layout')
    

@section('body')

    <body>



    @include('partials.navbar')

    <div class="wrapper">
        <div class="page-header" style="background-image: url('assets/img/login-image.jpg');">
            <div class="filter"></div>
                <div class="container mt-4">
                   <div class="row mt-5">
                        <div class="col-md-12 d-flex justify-content-center ">
                            <div class="card card-register">
                                <h3 class="card-title">Register</h3>
                                <div class="social">
                          <div class="row">
                                    <div class="col-md-4">      <a href="#" class="nav-link btn-social btn-facebook btn-simple btn-footer">
                                        <i class="fa fa-facebook-square"></i>
                                    </a></div>
                                <div class="col-md-4">   <a href="#" class="nav-link  btn-social btn-twitter btn-simple btn-footer">
                                        <i class="fa fa-twitter"></i>
                                    </a></div>
                                <div class="col-md-4">
                                          <a href="#" class="nav-link  btn-social btn-google btn-simple btn-footer">
                                        <i class="fa fa-google"></i>
                                    </a></div>
                          </div>
                                 
                                       
                                </div>
                                 
                                <div class="division ">
                                    <div class="line l"></div>
                                    <span>or</span>
                                    <div class="line r"></div>
                                </div>
                              
                                <form class="register-form">
                                    <input type="text" class="form-control" placeholder="Email">

                                    <input type="password" class="form-control" placeholder="Password">

                                    <input type="password" class="form-control" placeholder="Confirm Password">

                                    <button class="btn btn-block btn-round"><i class="nc-icon nc-key-25"></i> Register</button>
                                </form>
                                <hr>
                                <div class="login">
                                    <p>Already have an account? <a href="#0">Log in</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                </div>
            </div>
    </div>

    
    @include('partials.footer')

    </body>




@endsection