<!-- Simple modal -->

    <div class="modal" id="noticeModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                                    <div class="modal-dialog modal-notice modal-lg pt-1">
                                        <div class="modal-content">
                                            <div class="modal-header no-border-header">
                                                <h5 class="modal-title" id="myModalLabel">Product Details</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="instruction">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p> <strong><h6 class="title">Front</h6></strong>  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos quod at laborum temporibus quidem.</p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="picture">
                                                                <img src="https://placehold.it/500x300" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="instruction">
                                                    <div class="row">
                                                     <div class="col-md-6">
                                                            <div class="picture">
                                                                <img src="https://placehold.it/500x300" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p> <strong><h6 class="title">Rear </h6></strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis aperiam at, repellendus amet? Obcaecati labore esse.</p>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary btn-link" data-dismiss="modal"><i class="nc-icon nc-minimal-left"></i> Return</button>
                                            </div>
                                        </div>
                                    </div>
    </div>