<!-- footer  -->
    <footer class="footer footer-big footer-color-black" data-color="black">
        <div class="container ">

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="info">
                        <h5 class="title">Compañia</h5>
                        <nav>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="{{URL::route('index')}}">Inicio</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{URL::route('about')}}">Quiénes somos</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{URL::route('privacy')}}">Política de la Compañia</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="info">
                        <h5 class="title">Ayuda &amp; Soporte</h5>
                        <nav>
                            <ul class="list-group">
                                <li  class="list-group-item">
                                    <a href="{{URL::route('contact')}}">Contactenos</a>
                                </li>
                                <li  class="list-group-item">
                                    <a href="{{URL::route('terms')}}">Terminos &amp; Condiciones</a>
                                </li>
                                <li  class="list-group-item">
                                    <a href="{{URL::route('refund')}}">Devoluciones</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="info">
                        <h5 class="title">Noticias Recientes</h5>
                        <nav>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#">
                                        <i class="fa fa-twitter"></i> Lorem ipsum dolor sit amet, consectetur  elit. 
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>Lorem ipsum dolor sit amet delectus.
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="info">
                        <h5 class="title">Síguenos</h5>
                        <nav>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#" class="nav-link btn-social btn-facebook btn-simple btn-footer">
                                        <i class="fa fa-facebook-square"></i> Facebook
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="#" class="nav-link  btn-social btn-twitter btn-simple btn-footer">
                                        <i class="fa fa-twitter"></i> Twitter
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#" class="nav-link  btn-social btn-instagram btn-simple btn-footer">
                                        <i class="fa fa-instagram"></i> Instagram
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="media no-underline pb-0" >
                      <a href="{{URL::route('news')}}"><img class="d-flex mr-3 rounded" src="assets/img/hiker_thumb.jpg" alt="Generic placeholder image"></a>
                      <div class="media-body">
                        <h5 class="text-danger mt-0">Reaching the peak</h5>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nemo placeat laboriosam, reprehenderit quos vitae quod qui animi, rerum voluptas quis repellat aspernatur libero beatae. Rem, dolor laboriosam natus ex.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="media no-underline pb-0">
                    <a href="{{URL::route('news')}}"><img class="d-flex mr-3 rounded" src="assets/img/climbing__5__thumb.jpg" alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h5 class="text-danger mt-0">Walking out the confort zone</h5>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure, dignissimos adipisci suscipit hic incidunt repellat porro veniam alias officiis. Labore, accusamus, accusantium. Corrupti praesentium blanditiis eos minima, labore ex!
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="copyright">
           © <script> document.write(new Date().getFullYear()) </script> Krewbit <i class="fa fa-desktop"></i> Ingeniería Digital
       </div>
        </div>
    </footer>
<!-- end footer -->