
<!--    navbar           -->

<nav class="navbar navbar-toggleable-md fixed-top navbar-transparent bg-danger" color-on-scroll="50">
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Marca - empresa</a>
        </div>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::route('index')}}" data-scroll="true">Inicio</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Secciones</a>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                        <li class="dropdown-item"><a href="{{URL::route('gallery')}}"><i class="nc-icon nc-album-2"></i>&nbsp; Galería</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('index')}}#portfolio"><i class="nc-icon nc-layout-11"></i>&nbsp; Portfolio</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('news')}}"><i class="nc-icon nc-paper"></i>&nbsp; Noticias</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('about')}}"><i class="nc-icon nc-badge"></i>&nbsp; Quiénes Somos</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('contact')}}"><i class="nc-icon nc-globe"></i>&nbsp; Contacto</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Usuario</a>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                        <li class="dropdown-item"><a href="{{URL::route('profile')}}"><i class="nc-icon nc-circle-10"></i>&nbsp;Cuenta</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('login')}}"><i class="nc-icon nc-lock-circle-open"></i>&nbsp;Iniciar Sesión</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('register')}}"><i class="nc-icon nc-key-25"></i>&nbsp; Suscribrirse</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('terms')}}"><i class="nc-icon nc-single-copy-04"></i>&nbsp; Terminos y Condiciones</a></li>
                        <li class="dropdown-item"><a href="{{URL::route('refund')}}"><i class="nc-icon nc-money-coins"></i>&nbsp; Devoluciones</a></li>

                    </ul>
                </li>   
                <li class="nav-item">
                    <a id="grayscale" href="{{ URL::route('store') }}" class="btn btn-danger btn-round"><i class="nc-icon nc-cart-simple"></i> Tienda</a> <!-- Store link -->
                    <a class="btn btn-round btn-danger" href="{{URL::route('news')}}"><i class="nc-icon nc-paper"></i> Noticias</a>                     <!-- News Link -->
                </li>   

                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="#" target="_blank">
                        <i class="fa fa-twitter"></i>
                        <p class="hidden-lg-up">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="#" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                        <p class="hidden-lg-up">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="#" target="_blank">
                        <i class="fa fa-instagram"></i>
                        <p class="hidden-lg-up">Instagram</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>


    <!-- end navbar  -->