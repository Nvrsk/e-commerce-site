
    <!-- Demo modal -->

    <div class="modal" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                                    <div class="modal-dialog modal-notice">
                                        <div class="modal-content">
                                            <div class="modal-header no-border-header">
                                                <h5 class="modal-title" id="myModalLabel">Frostbite Climbing Gear</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="instruction">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <p> <strong><h6 class="title">Jacket</h6></strong>  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos quod at laborum temporibus quidem.</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="picture">
                                                                <img src="assets/img/project_2_one.jpg" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="instruction">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <p> <strong><h6 class="title">Boots </h6></strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis aperiam at, repellendus amet? Obcaecati labore esse.</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="picture">
                                                                <img src="assets/img/project_2_two.jpg" alt="Thumbnail Image" class="img-rounded img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary btn-link" data-dismiss="modal"><i class="nc-icon nc-minimal-left"></i> Return</button>
                                            </div>
                                        </div>
                                    </div>
    </div>
    <!-- Demo modal -->