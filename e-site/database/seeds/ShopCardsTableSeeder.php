<?php

use Illuminate\Database\Seeder;

class ShopCardsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shop_cards')->delete();
        
        \DB::table('shop_cards')->insert(array (
            0 => 
            array (
                'id' => 1,
                'class' => 'Shoes',
                'genre' => 'male',
            'thumb' => 'shoe-(1)',
                'name' => 'Pathfinder Boots',
                'price' => '2.90',
                'type' => 'shoes',
            ),
            1 => 
            array (
                'id' => 3,
                'class' => 'Jackets',
                'genre' => 'male',
            'thumb' => 'jacket (8)',
                'name' => 'Chrystal Windbreaker',
                'price' => '1.50',
                'type' => 'clothes',
            ),
            2 => 
            array (
                'id' => 4,
                'class' => 'Shoes',
                'genre' => 'female',
            'thumb' => 'shoe-(3)',
                'name' => 'Hilltop Shoes',
                'price' => '3.50',
                'type' => 'shoes',
            ),
            3 => 
            array (
                'id' => 5,
                'class' => 'Jackets',
                'genre' => 'female',
            'thumb' => 'jacket (4)',
                'name' => 'Stretch-Knit Jacket',
                'price' => '2.60',
                'type' => 'clothes',
            ),
            4 => 
            array (
                'id' => 6,
                'class' => 'Jackets',
                'genre' => 'female',
            'thumb' => 'jacket (7)',
                'name' => 'Glass Beads Vest',
                'price' => '7.50',
                'type' => 'clothes',
            ),
            5 => 
            array (
                'id' => 7,
                'class' => 'Jackets',
                'genre' => 'male',
            'thumb' => 'jacket (1)',
                'name' => 'Grey Velvet Windbreaker',
                'price' => '1.30',
                'type' => 'clothes',
            ),
            6 => 
            array (
                'id' => 9,
                'class' => 'Jackets',
                'genre' => 'female',
            'thumb' => 'jacket (6)',
                'name' => 'Purple Orchid Climbing Coat',
                'price' => '3.70',
                'type' => 'clothes',
            ),
            7 => 
            array (
                'id' => 10,
                'class' => 'Shoes',
                'genre' => 'male',
            'thumb' => 'shoe-(2)',
                'name' => 'Ash Lake Hikers',
                'price' => '4.50',
                'type' => 'shoes',
            ),
        ));
        
        
    }
}