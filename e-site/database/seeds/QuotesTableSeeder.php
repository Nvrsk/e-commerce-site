<?php

use Illuminate\Database\Seeder;

class QuotesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('quotes')->delete();
        
        \DB::table('quotes')->insert(array (
            0 => 
            array (
                'id' => 0,
                'title' => 'quote',
                'body' => 'Si tu negocio no está en Internet, no existe.',
                'author' => 'Bill Gates',
                'CO' => 'Microsoft',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1,
                'title' => 'quote',
                'body' => 'Ve en grande o vete a casa.',
                'author' => 'Anónimo',
                'CO' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}