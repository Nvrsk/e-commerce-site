<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

	           //          Seed DB through dump file

	        	  //       $tabledata = DB::table('table_name')->get();                      //To retrieve data from DB

	           //  		$encodedTableData = json_encode($tabledata,TRUE);           		//From array to JSON file

	           //  		file_put_contents('table_name.txt', $encodedTableData);     		//To save JSON file

	        	 	// 	$JSONfile = file_get_contents('table_name.txt');            		//To retrieve JSON file 

	        			// $decodedTableData = json_decode($JSONfile, true);           		//To retrieve data JSON file 

	           //          DB::table('table_name')->insert($decodedTableData);             	//To inject data from JSON file to DB




        
        $this->call(HeadercardsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ShopCardsTableSeeder::class);
        $this->call(QuotesTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
    }
}
