<?php

use Illuminate\Database\Seeder;

class HeadercardsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('headercards')->delete();
        
        \DB::table('headercards')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Galería',
                'type' => 'store',
                'icon' => 'nc-album-2',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:33:21',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Portafolio',
                'type' => 'store',
                'icon' => 'nc-layout-11',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:34:00',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Testimonios',
                'type' => 'store',
                'icon' => 'nc-chat-33',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:35:00',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Tienda',
                'type' => 'store',
                'icon' => 'nc-cart-simple',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:35:27',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Ofertas',
                'type' => 'service',
                'icon' => 'nc-alert-circle-i',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:37:14',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Promociones',
                'type' => 'service',
                'icon' => 'nc-bookmark-2',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:37:47',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Paquetes',
                'type' => 'service',
                'icon' => 'nc-box-2',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:38:22',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Productos',
                'type' => 'service',
                'icon' => 'nc-bullet-list-67',
                'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores veniam.',
                'link' => 'Ver Más',
                'created_at' => NULL,
                'updated_at' => '2017-09-24 19:38:45',
            ),
        ));
        
        
    }
}