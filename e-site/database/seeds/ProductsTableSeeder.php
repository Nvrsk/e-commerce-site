<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'img' => 'project_1_portfolio',
                'class' => 'service',
                'title' => 'Vail Boards',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'img' => 'project_2',
                'class' => 'gear',
                'title' => 'Frostbite Climbing Gear',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'img' => 'project_3',
                'class' => 'product',
                'title' => 'Custom Skis',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'img' => 'project_4',
                'class' => 'product',
                'title' => 'High Tech Enviroment Footwear',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'img' => 'project_5',
                'class' => 'product',
                'title' => 'Premium Equipment',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ad iusto ea dolor illum nam vitae facere asperiores animi qui nisi nemo explicabo ratione vero amet beatae ex, alias cupiditate.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}