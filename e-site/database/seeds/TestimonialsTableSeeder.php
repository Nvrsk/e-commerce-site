<?php

use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('testimonials')->delete();
        
        \DB::table('testimonials')->insert(array (
            0 => 
            array (
                'id' => 1,
                'thought' => '"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a."',
                'name' => 'Gina Andrew',
                'social' => '@ginaandrew',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'thought' => '"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a."',
                'name' => 'Joe West',
                'social' => '@georgewest',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'thought' => '"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo expedita nihil assumenda debitis autem odit adipisci eveniet quidem voluptate tenetur doloribus animi quam nobis a."',
                'name' => 'Alec Thompson',
                'social' => '@alecthompson',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}