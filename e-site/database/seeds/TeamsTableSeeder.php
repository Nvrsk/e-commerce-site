<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teams')->delete();
        
        \DB::table('teams')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sophie West',
                'role' => 'CEO',
                'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Durk Jones',
                'role' => 'Director',
                'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Robert Orben',
                'role' => 'Ventas',
                'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis sed, error itaque ad voluptatem aspernatur delectus perspiciatis beatae assumenda.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}