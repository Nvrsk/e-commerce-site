<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_cards', function (Blueprint $table) {

            $table->increments('id');

            $table->string('class');

            $table->string('genre');

            $table->string('thumb');

            $table->string('name');

            $table->decimal('price');

            $table->string('type');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_cards');
    }
}
